#!/bin/sh -eux

SPARK=spark-$SPARK_VERSION-bin-hadoop$HADOOP_VERSION

tar xzvf /tmp/$SPARK.tgz -C /opt/
ln -s /opt/$SPARK /opt/spark

cat <<EOF >/etc/profile.d/spark.sh
export SPARK_HOME=/opt/spark
export PATH=\$PATH:\$SPARK_HOME/bin
EOF

