#!/bin/sh -eux

# ZEPPELIN_VERSION=0.7.2
ZEPPELIN=zeppelin-$ZEPPELIN_VERSION-bin-netinst


# Unzip and place the zeppelin files into /opt
tar xzvf /tmp/$ZEPPELIN.tgz -C /opt/
ln -s /opt/$ZEPPELIN /opt/zeppelin

# Zeppelin environment variables
cat <<EOF >/etc/profile.d/zeppelin.sh
export ZEPPELIN_HOME=/opt/zeppelin
export PATH=\$PATH:\$ZEPPELIN_HOME/bin

export PYTHONPATH=\$SPARK_HOME/python:\$PYTHONPATH
export PYTHONPATH=\$SPARK_HOME/python/lib/py4j-0.10.4-src.zip:\$PYTHONPATH 

# Let us use external python scripts from inside zeppelin
export PYTHONPATH=$PYTHONPATH:/vagrant/

# Let the notebooks live outside vagrant so that the code can be versioned, exported etc.
export ZEPPELIN_NOTEBOOK_DIR=/vagrant

EOF

# Install the interpreters needed
/opt/zeppelin/bin/install-interpreter.sh --name md,python

# The vagrant user is one that runs zeppelin, so change ownership to vagrant
chown -R vagrant:vagrant /opt/$ZEPPELIN /opt/zeppelin

# Install python data science libs also
sudo apt install -y python-numpy python-matplotlib python-pandas python-scipy
