#!/bin/sh -eux

# Provisioning script to install kafka 0.10.2.0 built against scala 2.11

# For debugging. This values should come from packer
# KAFKA_VERSION=0.10.2.0
# SCALA_VERSION=2.11

KAFKA=kafka_$SCALA_VERSION-$KAFKA_VERSION

# untar to opt
tar xzvf /tmp/$KAFKA.tgz -C /opt

# soft link to an easy to remember name
ln -s /opt/$KAFKA /opt/kafka

# Copy our kafka config file on to the config folder
cp /tmp/server.properties /opt/$KAFKA/config

# every thing should run under the user kafka
groupadd kafka
useradd -u 10000 -g kafka -d /home/kafka -s /bin/bash -p '$da1057ef-cb28-42c2-853b-ed563a7cabf8' kafka

# change the ownership of the all kafka related dirs to kafka:kafka
chown -R kafka:kafka /opt/$KAFKA /opt/kafka

# Add service for both zookeeper and kafka
cp /tmp/*.service /etc/systemd/system

systemctl daemon-reload

sleep 10

# Enable both services for startup on boot
systemctl enable zookeeper.service 
systemctl enable kafka.service


