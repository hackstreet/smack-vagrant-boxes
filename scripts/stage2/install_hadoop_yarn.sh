#!/bin/sh -eux


# For debugging. Values should come from packer
# HADOOP_VERSION=2.8.0
# CONFIGURE_YARN="false"

HADOOP=hadoop-$HADOOP_VERSION
HOME=/home/vagrant


# Set up the java path
cat <<EOF >/etc/profile.d/J00-java-path.sh
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
EOF

# Set up hadoop paths
cat <<EOF >/etc/profile.d/J01-hadoop-paths.sh
export HADOOP_HOME=/opt/hadoop

export HADOOP_COMMON_HOME=\$HADOOP_HOME
export HADOOP_CONF_DIR=\$HADOOP_HOME/etc/hadoop
export HADOOP_HDFS_HOME=\$HADOOP_HOME
export HADOOP_MAPRED_HOME=\$HADOOP_HOME
export HADOOP_YARN_HOME=\$HADOOP_HOME

export PATH=\$PATH:\$HADOOP_HOME/bin:\$HADOOP_HOME/sbin
EOF

# For passwordless login into localhost
ssh-keygen -t rsa -P '' -f $HOME/.ssh/id_rsa
cat $HOME/.ssh/id_rsa.pub >> $HOME/.ssh/authorized_keys
chmod 0600 $HOME/.ssh/authorized_keys
chown -R vagrant:vagrant $HOME/.ssh

# untar binaries into /opt/
tar xzvf /tmp/$HADOOP.tar.gz -C /opt

# Soft link to an easy to remember name
ln -s /opt/$HADOOP /opt/hadoop

# Change ownership of all the files created obove to vagrant. We'll run hadoop under user vagrant
chown -R vagrant:vagrant /opt/$HADOOP /opt/hadoop

# Copy configuration files
cp /tmp/core-site.xml /opt/hadoop/etc/hadoop/
cp /tmp/hdfs-site.xml /opt/hadoop/etc/hadoop/
cp /tmp/hadoop-env.sh /opt/hadoop/etc/hadoop/


if [ $CONFIGURE_YARN = "true" ]; then
    cp /tmp/mapred-site.xml /opt/hadoop/etc/hadoop/
    cp /tmp/yarn-site.xml /opt/hadoop/etc/hadoop/
fi
