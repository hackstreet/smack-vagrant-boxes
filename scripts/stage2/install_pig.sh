#!/bin/sh -eux

# For debugging. For production builds values should come from packer.
# PIG_VERSION=0.17.0

PIG=pig-$PIG_VERSION

# Untar the pig binary into the opt folder
tar xzvf /tmp/$PIG.tar.gz -C /opt

# Soft link to /opt/pig
ln -s /opt/$PIG /opt/pig

# Change ownership to vagrant.
chown -R vagrant:vagrant /opt/$PIG /opt/pig
