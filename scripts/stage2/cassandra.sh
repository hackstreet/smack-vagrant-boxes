#!/bin/sh -eux


APT_USE_PROXY="true"

# If apt proxy is specified, install from proxy
if [ $APT_USE_PROXY = "true" ]; then

cat <<EOF >/etc/apt/apt.conf.d/02proxy;
Acquire::http::Proxy "$APT_HTTP_PROXY";
EOF

fi

# Add the cassandra PPA 
cat <<EOF >/etc/apt/sources.list.d/cassandra.sources.list
deb http://www.apache.org/dist/cassandra/debian 310x main
EOF

# Add the keys for cassandra devs
curl https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add -

# Update the repositories
sudo apt-get update

# Install java and cassandra
apt-get install -y openjdk-8-jdk-headless

# If we used a proxy to install apt packages, clean up after
if [ $APT_USE_PROXY = "true" ]; then
    rm -rf /etc/apt/apt.conf.d/02proxy
fi


apt-get install -y cassandra 
