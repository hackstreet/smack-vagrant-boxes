#!/bin/sh -eux


# APT_USE_PROXY="true"

# If apt proxy is specified, install from proxy
if [ $APT_USE_PROXY = "true" ]; then

cat <<EOF >/etc/apt/apt.conf.d/02proxy;
Acquire::http::Proxy "http://192.168.0.4:3142/";
EOF

fi

# Disable release-upgrades
sed -i.bak 's/^Prompt=.*$/Prompt=never/' /etc/update-manager/release-upgrades;

# Update the package list
apt-get -y update;

# update package index on boot
cat <<EOF >/etc/init/refresh-apt.conf;
description "update package index"
start on networking
task
exec /usr/bin/apt-get update
EOF

# Disable periodic activities of apt
cat <<EOF >/etc/apt/apt.conf.d/10disable-periodic;
APT::Periodic::Enable "0";
EOF


# If we used a proxy to install apt packages, clean up after
if [ $APT_USE_PROXY = "true" ]; then
    rm -rf /etc/apt/apt.conf.d/02proxy
fi


# Upgrade all installed packages incl. kernel and kernel headers
apt-get -y dist-upgrade;
reboot;
sleep 60;
