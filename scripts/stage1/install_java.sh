#!/bin/sh -eux

# If apt proxy is specified, install from proxy
if [ $APT_USE_PROXY = "true" ]; then

cat <<EOF >/etc/apt/apt.conf.d/02proxy;
Acquire::http::Proxy "$APT_HTTP_PROXY";
EOF

fi

apt-get update

# Java is a dependency for cassandra
apt-get install -y openjdk-8-jdk-headless


# If we used a proxy to install apt packages, clean up after
if [ $APT_USE_PROXY = "true" ]; then
    rm -rf /etc/apt/apt.conf.d/02proxy
fi

