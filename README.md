# SMACK Stack Vagrant boxes #

Packer scripts to build Vagrant boxes geared towards learning the SMACK stack. This is a work in progress, and what I have here is only the initial version.

## List of Boxes ##


 1. Java - Base Box

    The base box for all the other SMACK vagrant boxes.

 2. Cassandra.

    Box with cassandra installed.

 3. Scala. Scala + Akka

    A box with just enough software installed to learn the Scala language, Akka and related libs.


 4. Kafka



## Using the boxes ##


## Building the boxes with packer ##


### Stage1 build ###

Since the build is a time/network intensve process, I'v broken the build into two stages. Stage1 takes the xenial iso an builds an .ovf out of the iso. It also ...
  * updates the OS
  * adds password less ssh, password less sudo
  * adds virtualbox extensions to the guest os
  * cleans up unnecessary apt packages, caches etc.

### Stage2 ###

 1. **JAVA**: Takes the stage1 ovf, installs java and builds a vagrant box. Also uploads the box to
 vagrant cloud.

 2. **CASSANDRA**: builds the cassandra vagrant box from stage1 ovf.

 3. **SCALA**: ~Todo~

 4. **KAFKA**: ~Todo~

## License ##

The code is mostly adaped from the bento packer scripts, and has exactly the same license.

## Refrence ##

 1. Bento packer templates

    http://chef.github.io/bento/
