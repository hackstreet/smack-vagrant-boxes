#!/bin/bash

# Copy manager specific (NameNode + ResourceManager) configs to VM
cp /tmp/configs/hdfs-site.xml-manager /opt/hadoop/etc/hadoop/hdfs-site.xml
cp /tmp/configs/yarn-site.xml-manager /opt/hadoop/etc/hadoop/yarn-site.xml
