#!/bin/bash


# Copy slave specific configs to VM
cp /tmp/configs/hdfs-site.xml-slave /opt/hadoop/etc/hadoop/hdfs-site.xml
cp /tmp/configs/yarn-site.xml-slave /opt/hadoop/etc/hadoop/yarn-site.xml
