#!/bin/bash

# Append host recoreds to hosts file. It's far easier to deal with names rather
# than ip addresses.
cat /tmp/configs/lab.hosts >> /etc/hosts

# Copy the config files for a working yarn cluster. These are common for
# managers and slave nodes.
cp /tmp/configs/J01-hadoop-paths.sh /etc/profile.d/

cp /tmp/configs/core-site.xml /opt/hadoop/etc/hadoop/
cp /tmp/configs/mapred-site.xml /opt/hadoop/etc/hadoop/

# Create dirs used by hadoop for storage. Change owner to vagrant.
mkdir -p /var/hadoop/hdfs/namenode
mkdir -p /var/hadoop/hdfs/datanode

chown -R vagrant:vagrant /var/hadoop
